package matricula.controller;

import java.io.Serializable;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import matricula.controller.util.JSFUtil;
import matricula.model.entities.Cliente;
import matricula.model.entities.Revision;
import matricula.model.entities.Vehiculo;
import matricula.model.managers.ManagerMatricula;

import java.util.List;

@Named
@SessionScoped
public class BeanMatricula implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Date anio_matricula;
    private String observaciones;
    private boolean paso_mtricula;
    private String id_cliente;
    private String id_vehiculo;
    private List<Cliente> listaClientes;
    private List<Vehiculo> listaVehiculos;
    private List<Revision> listaMatriculas;
    
    @EJB
    private ManagerMatricula managerMatricula;

    /**
     * Creates a new instance of BeanMatricula
     */
    public BeanMatricula() {
    }    
    
    @PostConstruct
    public void Inicializar() {
        listaClientes = managerMatricula.findAllCliente();
        listaMatriculas = managerMatricula.findAllMatriculas();
        listaVehiculos = managerMatricula.findAllVehiculos();
    }
    
    public void actionListenerGuardarMatricula() {
        if (managerMatricula.registrarNuevaMatricula(anio_matricula, observaciones, paso_mtricula, id_cliente, id_vehiculo)) {
            listaMatriculas=managerMatricula.findAllMatriculas();
            //FacesMessage mensaje=new FacesMessage(FacesMessage.SEVERITY_INFO,"Selecciono revision de matricula exitosa",null);
            //FacesContext.getCurrentInstance().addMessage(null, mensaje);
            JSFUtil.crearMensajeInfo("Matricula guardado con exito!");
        } else {
        	//FacesMessage mensaje=new FacesMessage(FacesMessage.SEVERITY_ERROR,"Error",null);
            //FacesContext.getCurrentInstance().addMessage(null, mensaje);
            JSFUtil.crearMensajeError("Error al generar la matricula!");
        }
    }
    
    public void addMessage() {
        if (paso_mtricula) {
            //FacesMessage mensaje=new FacesMessage(FacesMessage.SEVERITY_INFO,"Selecciono revision de matricula exitosa",null);
            //FacesContext.getCurrentInstance().addMessage(null, mensaje);
            JSFUtil.crearMensajeInfo("Seleciono Revision de matricula exitosa!");
        } else {
            //FacesMessage mensaje=new FacesMessage(FacesMessage.SEVERITY_ERROR,"Error",null);
            //FacesContext.getCurrentInstance().addMessage(null, mensaje);
        	JSFUtil.crearMensajeError("Selecciono Revision de matricula con errores. No pasa la matricula!");
        }
    }

	public Date getAnio_matricula() {
		return anio_matricula;
	}

	public void setAnio_matricula(Date anio_matricula) {
		this.anio_matricula = anio_matricula;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public boolean isPaso_mtricula() {
		return paso_mtricula;
	}

	public void setPaso_mtricula(boolean paso_mtricula) {
		this.paso_mtricula = paso_mtricula;
	}

	public String getId_cliente() {
		return id_cliente;
	}

	public void setId_cliente(String id_cliente) {
		this.id_cliente = id_cliente;
	}

	public String getId_vehiculo() {
		return id_vehiculo;
	}

	public void setId_vehiculo(String id_vehiculo) {
		this.id_vehiculo = id_vehiculo;
	}

	public List<Cliente> getListaClientes() {
		return listaClientes;
	}

	public void setListaClientes(List<Cliente> listaClientes) {
		this.listaClientes = listaClientes;
	}

	public List<Vehiculo> getListaVehiculos() {
		return listaVehiculos;
	}

	public void setListaVehiculos(List<Vehiculo> listaVehiculos) {
		this.listaVehiculos = listaVehiculos;
	}

	public List<Revision> getListaMatriculas() {
		return listaMatriculas;
	}

	public void setListaMatriculas(List<Revision> listaMatriculas) {
		this.listaMatriculas = listaMatriculas;
	}
    
  

}

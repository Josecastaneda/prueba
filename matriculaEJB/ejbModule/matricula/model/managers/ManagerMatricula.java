package matricula.model.managers;

import java.util.Date;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import matricula.model.entities.*;
/**
 * Session Bean implementation class ManagerMatricula
 */
@Stateless
@LocalBean
public class ManagerMatricula {
	@PersistenceContext
    private EntityManager entityManager;

    public List<Cliente> findAllCliente() {
        return entityManager.createNamedQuery("Cliente.findAll", Cliente.class).getResultList();
    }

    public List<Vehiculo> findAllVehiculos() {
        return entityManager.createNamedQuery("Vehiculo.findAll", Vehiculo.class).getResultList();
    }

    public List<Revision> findAllMatriculas() {
        return entityManager.createNamedQuery("Revision.findAll", Revision.class).getResultList();
    }

    public boolean registrarNuevaMatricula(Date anio_matricula, String observaciones, boolean paso_mtricula, String id_cliente, String id_vehiculo) {
        boolean resp = false;
        Cliente cliente = entityManager.find(Cliente.class, id_cliente);
        Vehiculo vehiculo = entityManager.find(Vehiculo.class, id_vehiculo);
        if (cliente != null && vehiculo != null) {
        	Revision revision = new Revision();
        	revision .setAnioRevision(anio_matricula);
        	revision .setObservaciones(observaciones);
        	revision .setPasoRevision (paso_mtricula);
        	revision .setClienteBean(cliente);
        	revision .setVehiculoBean(vehiculo);
            entityManager.persist(revision);
            resp=true;
        }
        return resp;
    } 

}
